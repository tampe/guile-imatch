(define-module (ice-9 imatch)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:export (dmatch imatch append-imatch prepend-imatch imatch-null
                   dmatch append-imatch prepend-imatch dmatch-null))

#|
A functional matcher and indexed matcher for lare matchers use the index
matcher imatch else for small problem the sequential dmatch

pattern a symbol ?a represents a variable and the rest is atoms list and
vectors.

The emty imatch is imatch-null, the empty dmatch is dmatch-null. We can add a matcher in front of the structure or at the end. The api for this is

dmatch <- (append-dmatch  pat lambda dmatch)
dmatch <- (prepend-dmatch pat lambda dmatch)

imatch <- (append-dmatch  pat lambda imatch)
imatch <- (prepend-dmatch pat lambda imatch)

lambd will at a match be fed with the variables defined in a depth first search
in the pattern. To apply the match use it as

(dmatch x dmatch-data #:optional (fail (lambda () (error "dmatch failed"))))
(imatch x imatch-data #:optional (fail (lambda () (error "imatch failed"))))
|#

(define (union          . l)  (apply lset-union        =   l))
(define (intersection x . l)  (apply lset-intersection = x l))

(define (compile-pattern pat vars)
  (match pat
    ((x . y)
     (call-with-values (lambda () (compile-pattern x vars))
       (lambda (x1 y1 vars)
         (call-with-values (lambda () (compile-pattern y vars))
           (lambda (x2 y2 vars)
             (values (list #:cons x1 x2)
                     (list #:cons y1 y2)
                     vars))))))
    
    (#(x ...)
     (call-with-values (lambda () (compile-pattern x vars))
       (lambda (x1 y1 vars)
         (values x1
                 (list #:vector (length x) y1)
                 vars))))

    (x
     (define (default-ret)
       (values (list #:atom x) (list #:atom x) vars))
     (if (symbol? x)
         (match (string->list (symbol->string x))
           ((#\? . _)
            (if (member x vars)
                (values #:var (list #:var x) vars)
                (values #:var (list #:var x) (cons x vars))))
           (_
            (default-ret)))
         (default-ret)))))
       

(define (match-0 pat x vars)
  (match pat
    ((#:var ?)
     (set-cdr! (assoc ? vars) x)
     vars)
    
    ((#:atom p)
     (equal? x p)
     vars)
    
    ((#:cons p1 p2)
     (match x
       ((x1 . x2)
        (let ((ret (match-0 p1 x1 vars)))
          (if ret
              (match-0 p2 x2 ret)
              ret)))))
    
    ((#:vector n ps)
     (if (vector? x)
         (if (= n (vector-length x))
             (match-0 ps (vector->list x) vars)
             #f)
         #f))))

(define (match-1 pat.vars x)
  (map cdr (match-0 (car pat.vars) x (map (lambda (x) (cons x #f))
                                           (cdr pat.vars)))))

(define dmatch-null (cons '() '()))


(define (prepend-dmatch pat lam dmatch)
  (let ((pre (car dmatch)))
    (call-with-values (lambda () (compile-pattern pat '()))
      (lambda (x1 x2 vars)
        (cons (cons (cons* lam x2 (reverse vars)) pre) (cdr dmatch))))))

(define (append-dmatch pat lam dmatch)
  (let ((pre (cdr dmatch)))
    (call-with-values (lambda () (compile-pattern pat '()))
      (lambda (x1 x2 vars)
        (cons (car dmatch) (cons (cons* lam pat (reverse vars)) pre))))))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define* (dmatch x dmatch #:optional (fail (lambda () (error "dmatch failed"))))
  (let lp ((l (append (car dmatch) (reverse (cdr dmatch)))))
    (match l
      (((lam . p.v) . l)
       (aif it (match-1 p.v x)
            (apply lam it)
            (lp l)))
      (()
       (fail)))))


(define index-null #f)

(define (compile-index pat index id)
  (define (new-index)
    (list #f #f vlist-null '()))
  
  (match index
    (#f
     (compile-index pat (new-index) id))
    
    ((car1 cdr1 atoms vars)
     (match pat
       ((#:vector n p)
        (compile-index p index id))
       
       ((#:cons x1 x2)
        (let ((car2 (compile-index x1 car1 id))
              (cdr2 (compile-index x2 cdr1 id)))
          (list car2 cdr2 atoms vars)))
       
       (#:var
        (list car1 cdr1 atoms (cons id vars)))

       ((#:atom a)
        (let ((new-atoms
               (aif it (vhash-assoc a atoms)
                    (vhash-cons a (cons id (cdr it)) atoms)
                    (vhash-cons a (cons id '())      atoms))))
          (list car1 cdr1 new-atoms vars)))))))

(define (index-matches index x)
  (match index
    (#f '())
    ((car1 cdr1 atoms vars)
     (union vars
            (match x
              ((x1 . x2)
               (intersection
                (index-matches car1 x1)
                (index-matches cdr1 x2)))

              (#(x ...)
               (index-matches index (vector->list x)))

              (x
               (aif it (vhash-assoc x atoms)
                    (cdr it)
                    '())))))))
               
(define* imatch-null
  (list -1 1 index-null vlist-null))

(define (prepend-imatch pat lam imatch)
  (match imatch
    ((i n index data)
     (call-with-values (lambda () (compile-pattern pat '()))
       (lambda (index-p p v)
         (let ((m (cons* lam p v)))
           (list (- i 1) n
                 (compile-index index-p index i)
                 (vhash-cons i m data))))))))

(define (append-imatch pat lam imatch)
  (match imatch
    ((i n index data)
     (call-with-values (lambda () (compile-pattern pat '()))
       (lambda (index-p p v)
         (let ((m (cons* lam p v)))
           (list i (+ n 1)
                 (compile-index index-p index n)
                 (vhash-cons n m data))))))))


(define* (imatch x imatch #:optional (fail (lambda () (error "no imatch"))))
  (match imatch
    ((i n index data)
     (let ((l (sort (index-matches index x) <)))
       (let lp ((l l))
         (match l
           ((i . l)
            (aif it (vhash-assoc i data)
                 (match it
                   ((_ lam . p.v)
                    (aif it (match-1 p.v x)
                         (apply lam it)
                         (lp l))))
                 (lp l)))
           (()
            (fail))))))))
